# River Water Quality (RSMA), South-East Interceptor Works, Nov. 2015

Water quality data for the St. Lawrence River resulting from the special program planned and carried out by the Aquatic Environment Monitoring Network as part of the work that required the closure of the southeast interceptor in November 2015. 

[Original Source](https://open.canada.ca/data/en/dataset/1c47aacb-9650-4e28-9097-99f4840c2b56)

Licence: [Creative Commons 4.0 Attribution (CC-BY) licence -- Quebec](https://www.donneesquebec.ca/licence/)
